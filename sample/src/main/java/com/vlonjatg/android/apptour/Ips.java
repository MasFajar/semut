package com.vlonjatg.android.apptour;

import android.app.ListActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

public class Ips extends ListActivity {
    String [] Ips ={"Matematika", "Ekonomi", "Sosiologi", "Geografi","Bahasa Indonesia","Bahasa Inggris"};

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ips);

        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Ips));

    }
}
