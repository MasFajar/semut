package com.vlonjatg.android.apptour;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

@SuppressWarnings("deprecation")
public class TabView extends TabActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_main);

        TabHost tabhost = getTabHost();
        TabHost.TabSpec spec;
        Intent intent;

        intent = new Intent().setClass(this, Ipa.class);//content pada tab yang akan kita buat
        spec = tabhost.newTabSpec("IPA").setIndicator("IPA", null).setContent(intent);//mengeset nama tab dan mengisi content pada menu tab anda.
        tabhost.addTab(spec);//untuk membuat tabbaru disini bisa diatur sesuai keinginan anda

        intent = new Intent().setClass(this, Ips.class);
        spec = tabhost.newTabSpec("IPS").setIndicator("IPS", null).setContent(intent);
        tabhost.addTab(spec);

        intent = new Intent().setClass(this, Ipa.class);
        spec = tabhost.newTabSpec("Bahasa").setIndicator("Bahasa", null).setContent(intent);
        tabhost.addTab(spec);
    }
}
